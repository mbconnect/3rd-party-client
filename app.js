var express = require('express');
var finalhandler = require('finalhandler');
var bodyparser = require("body-parser");
var request = require('request');
var jwt = require('jsonwebtoken');
var app = express();

// serve static files from 'client' subfolder
app.use(express.static('client'));

app.use(bodyparser.urlencoded({ extended: false }));

// request a UUID
app.post('/requestAT', function (req, res) {

    var sub = req.body.sub;
    var aud = req.body.aud;

    var options = {
        url: 'https://api-dev.corpinter.net/ats/ActionToken/',
        method: 'POST',
        headers: {
            'apikey': 'P8TKV331m2laiqBZIKRGBwHm71kveIzt'
        },
        json: true,
        body: {
            'sub': sub,
            'aud': aud
        }
    };

    function callback(error, response, body) {
        if (!error && response.statusCode == 202) {
            console.log("========================================");
            console.log("Successfully requested UUID for Action Token!");
            console.log(body);
            console.log("========================================");
            res.send(body);
        } else {
            console.log("========================================");
            console.log("Error while requesting Action Token!");
            console.log("Server response code was: HTTP " + response.statusCode);
            console.log(response.body);
            console.log("========================================")
        }
    }

    if (!sub || !aud) {
        res.status('400').send('Sub and aud are needed.');
    } else {
        console.log('Sending "Request Action Token" action with\nsub=' + sub + "\naud=" + aud);

        request(options, callback);
    }
});


// request the actual Action Token with a given UUID
app.post('/retrieveAT', function (req, res) {

    var uuid = req.body.uuid;

	var options = {
		url: 'https://api-dev.corpinter.net/ats/ActionToken/' + uuid,
		method: 'GET',
		headers: {
			'apikey': 'P8TKV331m2laiqBZIKRGBwHm71kveIzt'
		}
	};

	function callback(error, response, body) {
		if (!error && response.statusCode == 200) {
			//clearInterval(interval);
			console.log("========================================");
			console.log("Successfully retrieved Action Token!");
			console.log(body);
			console.log("========================================");
			var result = {'jwt_encoded': body, 'jwt_decoded': jwt.verify(body, "sharedsecret", { ignoreExpiration: true } )};
			res.send(result);
		} else {
			console.log("========================================");
			console.log("Error while retrieving Action Token!");
			console.log("Server response code was: HTTP " + response.statusCode);
			console.log(response.body);
			console.log("========================================");
			if (response.body == "Authentication failed") {
			    res.sendStatus(403);
            } else {
                res.status('500').send(body);
            }
		}
	}


    if (!uuid) {
        res.status('400').send('No UUID supplied.');
    } else {
        console.log('Sending "Request Action Token" action with uuid=' + uuid);
        request(options, callback);
    }

});

app.set('port', process.env.PORT || 5000)
app.listen(app.get('port'), function () {
  console.log('MBconnect REST demonstrator running on port ' + app.get('port') + '!');
});
